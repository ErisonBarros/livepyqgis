# Live 5: Criando um script *standalone*

**[![Live pyqgis 5 - Stallone script](https://img.youtube.com/vi/Kz-gaVz7qF4/0.jpg)](https://www.youtube.com/watch?v=Kz-gaVz7qF4)**  

[Live da Issue 14](https://gitlab.com/geocastbrasil/livepyqgis/issues/14)  

Nesta live, usaremos dados que foram organizados específicamente para a live e por isso, vocês terão que baixar [desta pasta do Google Drive](https://drive.google.com/drive/folders/1BIb1sCd2G4qPLqrEYapQx-5w_gX5l1lC?usp=sharing). Basta baixar a pasta `dados` e incluí-la tal como está no repositório `livepyqgis`.  

## O que é um Script *standalone*  

## Desenvolvendo ferramenta de dados legado  

Dado um *path*:  

* Listar *layers* de um geopackage;
* A cada layer:
    - [X] Qual o nome;  
    - [X] Que tipo de geometria;  
    - [X] Qual SRC;  
    - [X] Quantas features;  
    - [X] Quantos atributos;  
* Transformá-lo em um script *standalone*: que permita executar e ter esses dados sem a necessidade de abrir o QGIS.  

## Criando a ferramenta no QGIS

A ferramenta a ser desenvolvida, intensionalmente, é bem básica e será desenvolvida com tudo o que já vimos nas lives anteriores. Com isso simplificamos a parte de criação da solução e podemos nos focar no tema central,  criação de um script *standalone*.  

Basicamente, o que faremos é:

1. Definir o *path* e o geopackage (*gpkg*) a ser analisado;  
1. Caso o *gpkg* exista:  
    1. Vamos carregar o *gpkg*;
    1. Identificar todas as layers existentes e a cada uma delas, garregá-las para poder identificar:  
        1. o nome;  
        1. a quantidade de feções;  
        1. a quantidade de atributos;  
        1. o tipo de geometria;  
        1. o SRC;  
1. Caso o *gpkg* não exista:
    1. Indica erro;  

Veja que dentro do teste (item 2), vamos criar um loop para iterar a cada layer exitentes no *gpkg* em questão; E intensionalmente já estruturei a proposta (a ideia/modelo) da ferramenta seguindo esse direcionamento. Assim, como já deixo claro o que deverá acontecer caso o teste (item 2) tenha resultado negativo (*False*).  

:warning: Por questões didáticas, vamos apresentar o script em partes para que possamos discutir os pontos principais. A versão final poderá ser vista no [script.py](/estudos/live_5/script.py).  
  
Vamos ao desenvolvimento:

```python
from os import path

# definindo path ate a pasta do repositorio
diretorio = "path_to_livepyqgis"

# criando caminho ao geopackage
full_path = path.join(diretorio, 'dados/base_dados.gpkg')

# testando se geopackage existe
if path.isfile(full_path):
    arquivo = QgsVectorLayer( full_path, "test", "ogr")
    # Conectando ao gpkg com dataProvider()
    subLayers = arquivo.dataProvider().subLayers()
```  

Até o momento, tudo o que fizemos já foi apresentando em alguma das *lives* anteriores (principlmente, [live 2](estudos/live_5/README.md), sobre como conectar a um geopackage).  
Agora que temos um objeto `sublayers` com a lista de layers salvas no *geopckage*, vamos criar um *loop* para interar a cada uma delas e poder carregárlas como `QgsVectorLayers` e extrair as informações básicas sobre elas:  

```python
(...)
    for subLayer in subLayers:
        # print( subLayer )
        nome = subLayer.split( "!!::!!" )[1]
        print( "Nome da layer =", nome )
        uri = f"{full_path}|layername={nome}" # Se o python for 3.6
        # Cria Camada
        sub_vlayer = QgsVectorLayer( uri, nome, 'ogr' )
        # quantas feicoes
        print( "Total de feições =", sub_vlayer.featureCount())
        # Quantidade de Campos
        qtd_campos = len(sub_vlayer.fields())
        print("Quantidade de campos =", qtd_campos)
        # qual geometria
        print( "Geometria =", QgsWkbTypes.displayString(sub_vlayer.wkbType()))
        # consulta crs
        print( "SRC =", sub_vlayer.crs().authid(), "\n\n")
```

Neste processo é interessante comentar algumas coisas. Como apenas queremos extrair informações das camadas e não visualizá-las, basta cerregá-las como `QgsVectorLayer`, sem importá-las ao projeto.  
Para acessar a informação da geometria da camada, tivemos que usar o método [`QgsWkbTypes`](https://qgis.org/pyqgis/3.4/core/QgsWkbTypes.html?highlight=wkbtype#module-QgsWkbTypes) da classe `QgsVectorLayer`, que nos retorna em formato binário o tipo de geometría.  
Mas como o retorno é binário, precisamos cornverte-lo a texto e para isso usamos o [`displayString`](https://qgis.org/pyqgis/3.4/core/QgsWkbTypes.html?highlight=displaystring#qgis.core.QgsWkbTypes.displayString) da classe [`QgsWkbTypes`](https://qgis.org/pyqgis/3.4/core/QgsWkbTypes.html?highlight=qgswkbtypes#module-QgsWkbTypes), que nos retorna en um formato textual o tipo de geometria passado como parâmetro do médoto (informação passada dentro do parêntesis).  

Para fechar o script, vamos informar caso o geopackage não exista, independente do motivo (que pode ser erro ao informar o *path*).  

```python
else:
    print( "Geopackage não existe\n\n" )
```  

Imagino que vocês tenham notado o `\n\n` na ultima linha, fechando a *string* de erro ou fechando a informação do SRC. O `\n` é usado para quebra de linha no python. Ao usar dois seguidos, estou quebrando duas linhas antes para garantir espaço entre as informações de uma layer e outra ou após o erro.  

Tendo esse código rodando sem erros, teremos o resultado:

```python
>>>Nome da layer = 0902_mg_sede_municipal_pto
Total de feições = 853
Quantidade de campos = 9
Geometria = Point
SRC = EPSG:4674


Nome da layer = 1104_mg_municipios_pol
Total de feições = 853
Quantidade de campos = 22
Geometria = MultiPolygon
SRC = EPSG:4674

(...)

Nome da layer = grade_landsat_mg
Total de feições = 34
Quantidade de campos = 7
Geometria = MultiPolygon
SRC = EPSG:4674
```

A versão final do script está disponível [aqui](/estudos/live_5/script.py).  

### Transformando em script standalone

Agora parte mais interessante dessa sessão, vamos transformar esse script em um script *standalone* (que possa ser executado sem a necessidade de abrir o QGIS).  
> :warning: Nunca salve seu script com o nome "qgis.py". (PyQGIS cookbook author[s])  

Para isso teremos que inserir, **apenas**, cinco novas linhas de código!  
São elas:

1. O import das clases e métodos usados no script (Ex. `QgsVectorLayer`);  
1. O caminho até os recursos do QGIS. Ou seja, onde ele está instalado;  
1. Criação de uma instância dos recursos do QGIS (onde definiremos se usaremos a interface gráfica ou não);  
1. Carregamento dos recursos do QGIS;  
1. E, por ultimo e, no fim do script, a finalizacão dos recursos do QGIS;  

Em resumo o script ficará com a seguinte estrutura:

```python
from qgis.core import QgsApplication
# configura caminho ao recursos QGIS
QgsApplication.setPrefixPath("/path_to_qgis_installation", True)

# Cria referencia a aplicação QGIS
# Segundo argumento desabilita Interface grafica.
qgs = QgsApplication([], False)

# Inicializando e carregando os recursos
qgs.initQgis()

# Segue com o seu script

# por fim
# fecha recursos
qgs.exitQgis()
```

Mas vamos comentando cada passo e inserindo exatamente o que necessitaremos para o nosso script.  

Importando as classes: No presente exemplo precisaremos apenas importar algumas classes do `qgis.core`, como o `QgsVectoLayer`, `QgsApplication` e `QgsWkbTypes` (já vamos comentar os dois primeiros).

```python
from os import path
from qgis.core import ( QgsVectorLayer, QgsApplication, QgsWkbTypes)

# configura caminho ao recursos QGIS
QgsApplication.setPrefixPath('/home/user//qgis', True)
```

Vamos usar a classe [`QgsApplication`](https://qgis.org/pyqgis/3.4/core/QgsApplication.html?highlight=qgsapplication%20setprefixpath#module-QgsApplication) para permitir ao nosso script acessar/instanciar os recursos do QGIS sem a necessidade de te-lo aberto. E neste sentido, vamos configurar o *path* aos recursos do QGIS com o método [`setPrefixPath`](https://qgis.org/pyqgis/3.4/core/QgsApplication.html?highlight=qgsapplication%20setprefixpath) onde informaremos o *path* de onde está o QGIS. O  segundo parâmetro é *booleano* informando se deverá ser usado o *path* padrão.

:warning: Para saber o *path* correto ao QGIS, basta executar no terminal python do QGIS:

```python
>>> QgsApplication.prefixPath()
'/path_to_QGIS/'  
```  

A *string* retornada será usada na confirguração do `setPrefixPath()`, como mencionado anteriomente.  

Agora que já temos configurado o *path* ao QGIS, podemos criar um objeto que será ainstancia do QGIS. E iniciá-lo, carregando os recursos a serem usados no script [`initQgis`](`https://qgis.org/pyqgis/3.4/core/QgsApplication.html?highlight=initqgis#qgis.core.QgsApplication.initQgis`):  

```python
# Cria referencia a aplicação QGIS
# Segundo argumento desabilita Interface grafica.
qgs = QgsApplication([], False)

# Inicializando e carregando os recursos
qgs.initQgis()
```  

À continuação, teremos o script criado na sessão anterior. E, ao fim, fechamos os recursos do QGIS, com [`exitQgis`](https://qgis.org/pyqgis/3.4/core/QgsApplication.html?highlight=exitqgis#qgis.core.QgsApplication.exitQgis):  

```python
diretorio = "path_to_livepyqgis"

# criando caminho ao geopackage
full_path = path.join(diretorio, 'dados/base_dados.gpkg')

# testando se ele existe
if path.isfile(full_path):
    arquivo = QgsVectorLayer( full_path, "test", "ogr")

(...)

else:
    print( "nao deu" )

# fecha recursos
qgs.exitQgis()
```

A versão final do script *standalone* está disponível [aqui](/estudos/live_5/script2.py).  

Agora, para executar o script, basta executar pelo terminal:

```shell script
python3 path_to_livepyqgis/estudos/live_5/script2.py
```
