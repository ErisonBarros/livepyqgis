# Live 11: Ordem na Casa

![Live 11](https://i9.ytimg.com/vi/VpKrP8sOmv0/maxresdefault.jpg?time=1586577219425&sqp=COj8xPQF&rs=AOn4CLBSAGsq8LrLciP2kN1qHfTNS5uhPw)

[Link para a live](https://youtu.be/VpKrP8sOmv0)

Já tem um tempo que tenho vários colegas precisando de ordenar pontos em polígonos tendo como primeiro ponto o ponto mais ao norte. pois nem sempre durante a vetorização essa regra é seguida...
E outro problema que vejo muito também é os algorítimos não conseguirem remove os anéis internos de polígonos.  
A partir desses dois problemas vamos começar a entender como fazer limpeza nos dados que trabalhamos de forma programática.

Primeiro vamos ordenar os vértices.
Depois, vamos tentar eliminar anéis internos.

[Testes da classe QgsGeometry](https://github.com/qgis/QGIS/blob/1e05545fbcfa70532f85fd6657df8fef7f7cb01b/tests/src/python/test_qgsgeometry.py)

[Live Geometry Park](https://youtu.be/GlbIf8ZxpSE)

## Conhecendo algumas "baterias" do Python

## Sorted

O [`sorted()`](https://docs.python.org/3.7/howto/sorting.html) é utilizado para ordenar os valores dentro de uma lista, string e etc.  
No caso de ordenarmos uma string, o sorted gera uma lista com os valores separados.

```Python
l = "bcda"
sorted(l)
>>>["a", "b", "c", "d"]
n = [2, 3, 4, 1]
sorted(n)
>>> [1, 2, 3, 4]
p = ["charmander", "diglett" , "bulbasauro", "abra", ]
sorted(p)
>>> ['abra', 'bulbasauro', 'charmander', 'diglett']
# Chega de exemplos...
```

O `sorted` possui três parâmetros, um obrigatório e 2 opcionais.  

`sorted(iter, key, reverse)`

- `iter`: É o iterável que será ordenado, é obrigatório.  
- `key`: Quando queremos ordenar os items por uma chave específica (como um atributo por exemplo) utilizamos esse parâmetro.
- `reverse`: O padrão do sorted é ordenar de forma crescente, quando queremos inverter a ordem passamos esse valor como `True`, pois por padrão ele é `False`.  
Vamos ver o uso dos dois parametros opcionais mais a frente.

> Ahh não vai ter "exemplinho"??
> Nop!

### Collections namedtuple

```python
from collections import namedtuple
```

O pacote `collections` possui ferramentas muito úteis para trabalharmos no python, como novas formas de listas dicionários dentre outros.  
Aqui vamos utilizar a `namedtuple`, pois elas nos permite dar nomes aos itens dentro da lista e acessá-los através desses nomes.  
Para criamos uma tupla nomeada (A.K.A namedtuple - acho que agora fica mais fácil de entender.) precisamos de dar um nome à tupla e aos seus campos.  
Nossos campos serão `idx`, `x`, `y`, e vamos chamá-la de `Node`.
É necessário fazer o import do pacote no script, como demonstrado no inicio dessa parte, pois por padrão ela não vem importada.

```python
(...)
Node = namedtuple("Nodes", ["idx", "x", "y"])
```

Os nomes dos itens da tupla podem ser passados em uma lista, como feito acima, ou em uma string, separadas por espaços, como no exemplo abaixo.  

```python
(...)
Node = namedtuple("Nodes", "idx x y"])
```

Então, criando uma tupla com um `QgsPoint` por exemplo, podemos acessar os atributos da seguinte forma:

```Python
(...)
Ponto = QgsPoint(10, 20)
node = Node(1, Ponto.x(), Ponto.y())
node.idx
>>> 1
node.x
>>> 10
```

É por isso chama TUPLA NOMEADA.  
> AAahhhhhh entendiiiiiiii  
> Mas pq isso?  

Bom isso foi para facilitar um pouco para organizar os vértices, e depois vamos pegar esses vértices e cria uma lista de nós, usando a tupla `Node` que criamos.  

### Operator attrgetter

```Python
from operator import attrgetter
```

Mais uma coisa legal nas baterias inclusas do [Python](www.python.org) o `operator`.  
Dele vamos utilizar o `attrgetter` que nos permite pergar um determinado atributo durante uma iteração.  

## Pegando a lista de vertices

A geometria de uma feição é uma instância da classe `QgsGeometry`, ela possui um método, o `QgsGeometry.asPolygon()`.  
Isso faz com que a geometria seja transformada em uma lista de `QgsPoint()` na seguinte extrutura.  

```Python
# Organização interna de um poligono simples em forma de listas.
# Os pontos serão sempre do tipo QgsPoins()
[
    [pontos do anel externo da geometria separados por vírgula],
    [pontos do primeiro anel interno da geometria separados por vírgula], #Se houver
    [pontos do segundo anel interno da geometria separados por vírgula], #Se houver
    (...)
    [pontos do enésimo anel interno da geometria separados por vírgula] #Se houver
]
```

E é nessa primeira lista, que representa o anel externo, vamos chamá-lo de perímetro, que vamos trabalhar.  

Para acessarmos transfomarmos a geometria nessa lista, basta acessar a geometria da feição e chamar o método.

```Python
(...)
geometria = feature.geometry().asPolygon()
geom = geometria[0] # Pegando o primeiro anel de geom.
```

Para organizar os pontos do perímetro, vamos criar uma função para isso. Pois assim podemos usar essa função em outras partes, se preciso.  

```Python
(...)
def ordena_perimetro(perimetro):
    """Ordena o vetor do perímetro."""
    Node = namedtuple("Node", ["idx", "x", "y"])
    nodes = [Node(perimetro.index(i), i.x(), i.Y()) for i in perimetro] # Criando uma lista com os dados dos pontos do perímetro
```

Vamos agora brincar com a Tupla nomeada e com o attrgetter.  
Para usando o `sorted` e o `attrgetter` podemos ordenar os pontos através do atributo `y` (Latitude).

```Python
(...)
    ponto_mais_ao_norte = sorted(nodes, key=attrgetter("y"), reverse=true)
```

> QUE LEGAL, o `attrgetter('y')` vai pegar o atributo `x` que que criamos na tupla nomeada?  
> Exatamente, gafanhoto!

Mas nem tudo são flores, não é mesmo?  
> "Mas a vida, a vida é uma caixinha de surpresas" CLIMBER, Narrador de Joseph

Se simplesmente, pegarmos essa lista, e transfornar em uma geometria, vamos perceber que o desenho formado não é o mesmo.  
Isso é pelo fato de termos mudado a ordem dos pontos.

> Mas Kyle, se você já tem a geometria original em uma lista, poderia iniciar a nova lista a partir do ponto mais ao norte, e depois adicionar os outros, não?!?!  
> "Queria tanto ter um filho assim" NONATO, Professor Raimundo.

Então iniciamos a lista a partir do ponto ao norte (por isso pegamos o indice deles), e depois adicionamos os outros do inicio.  
Vamos modificar a linha do ponto mais ao norte da função para pegar apenas o indice do primeiro item. E logo em seguida criar a lista.

```Python
(...)
def ordena_perimetro(perimetro):
    """Ordena o vetor do perímetro."""
    Node = namedtuple("Node", ["idx", "x", "y"])
    nodes = [Node(perimetro.index(i), i.x(), i.Y()) for i in perimetro]
    ponto_mais_ao_norte = sorted(nodes, key=attrgetter("y"), reverse=true)[0].idx
    return [perimetro[ponto_mais_ao_norte:] + perimetro[1:ponto_mais_ao_norte]]
```

Na segunda lista começamos do item 1 pois o ultimo ponto é igual ao primeiro. Evitando ter pontos duplicados desnecessários.

## Re-montando a geometria

Agora vamos re-montar a geometria para que ela seja utilizada na feição. Para facilitar, vamos criar uma função que receba a o `QgsGeometry.geometry()` faça extraia o perimetro e passe para a função que criamos, e com o retorno da função, devolva um `QgsGeometry` novinho e organizado.

```Python
def ordena_poligono(geometria):
    geometria = geometria.asPolygon()
    geom = ordena_perimetro(geometria[0])
    geometria_ordenada = QgsGeometry.fromPolygonXY(geom)
    # Adicionando os anéis internos à geometria
    if len(geometria) > 1:
        for anel in geometria[:1]:
            geometria_ordenada.addRing(anel)
    return geometria_ordenada
```

> UAI! E esse IF ali??

O `if` acima vai adicionar os anéis internos que existiam na geometria original, caso não a gente não precise remover. Podemos por um parametro opcional na função, se quisermos remover os aneis in    ternos também, e colocar esse `if` dentro de outro `if`

```Python
def ordena_poligono(geometria, limpar_aneis=False):
    (...)
    if not limpar_aneis: # Se o parametro for verdadeiro, não entra no if
        if (...)
```

Bom... agora basta iterar sobre cada feição da nossa camada e usar a `função ordena_poligono()`:

```Python
# Pegando as capacidades da camada, pois nem toda camada vetorial tem a mesmas opções
caps = layer.dataProvider().capabilities()
layer.startEditing()
for feature in features:
    # Precisamos do id da feição para atualizar a geometria
    fid = feature.id()
    geom = ordena_poligono(feature.geometry())
    if caps & QgsVectorDataProvider.ChangeGeometries:
        layer.dataProvider().changeGeometryValues({fid : geom})
    else:
        print(f"Feição fid {fid} não pode ser modificada!")
layer.commitChanges() # Salva as alterações na camada e fecha a edição.
```

O metodo `changeGeometryValues()` precisa de um dicionário para fazer a modificação, basta ter o id da feição e a geometria nova.  
Podemos também criar um dicinário com os ids e geometrias as serem mudadas e passar esse dicionario para ele fora do `for`:

```Python
# Bonus Round
(...)
atualizar_geometrias = {}
for feature in features:
    fid = feature.id()
    geom = ordena_poligono(feature.geometry())
    atualizar_geometrias[fid] = geom

if caps & QgsVectorDataProvider.ChangeGeometries():
    layer.startEditing()
    layer.dataPovider().changeGeometriValues(atualizar_geometrias)
    layer.commitChanges()

else:
    print(f"Camada {layer.name()} não pode ser modificada!)
```

Pronto, temos nossos poligonos todos com o primeiro ponto o mais ao norte.

## Removendo anéis internos

Esse é mais fácil. Podemos criar uma função que pegue o `QgsGeometry` e não remonte a geometria com os anéis internos.

```Python
def limpa_anel(geometry):
    geom = geometry.asPoligon()
    return QgsGeometry.fromPolygonXY([geom[0]])
```

Pronto... o agora é iterar nas feições para modificar a geometria, igual fizemos com a ordenação dos pontos, mas sem a parte do `if`.
