# Live 2: Aprendendo no sufoco... 
  
**[![Live pyqgis 2 - Aprendendo no sufoco](https://img.youtube.com/vi/eZHz4SXR8R4/0.jpg)](https://youtu.be/eZHz4SXR8R4)**  

:warning: Nesta live, usaremos dados que foram organizados específicamente para a live e por isso, vocês terão que baixar [desta pasta do Google Drive](https://drive.google.com/drive/folders/1BIb1sCd2G4qPLqrEYapQx-5w_gX5l1lC?usp=sharing). Basta baixar a pasta `dados` e incluí-la tal como está no repositório `livepyqgis`. :warning:  

# Estruturas de dados

No Python existem diversos tipos de estruturas de dados, com os quais iremos nos deparar a partir dessa aula.  
Através da estrutura de dados podemos armazenar e recuperar informações, de forma organizadam dentro de um conjunto.  

> Professor, quando vou usar conjuntos na vida?  
> Na live de PyQGIS do Geocast Brasil.  

## Lista

A lista é uma das formas mais usadas de estruturas, e a principal também.  
É uma sequencia de dados, e a ordem é de acordo com a entrada de dados.  
E podem sofrer mudanças (adição/remoção de itens, ordenamento).
iniciam e terminam usando chaves `[]`.  
Os elementos da lista são acessados de acordo com o índice dele dentro da lista:  

```python
# lista de compras
comprasCasa = [ 'Arroz' , 'Feijão' , 'Macarrão' , 'Doce de leite Viçosa' , 'Queijo Minas' ]
```

Os itens dentro de uma lista podem ser acessados, informando a sua posição (indice) dentro dela, sendo que no pyton, os indices sempre iniciam no 0:  

```python
comprasCasa[0]
>> Arroz
comprasCasa[4]
>> Queijo Minas
```

> Onde será que já vimos isso antes heim??  
> NO `QgsProject.instance().mapLayersByName( 'Nome da camada' )[ 0 ]`  
> Shiuuuu, deixa eles responderem....

Na medida que outas estruturas forem aparecendo, a gente vai explicando.  

# Trabalhando com RASTERS

> Nem só de vetores vive aquele que trabalha com geoprocessamento, mas também de rasters

Chega um momento no nosso dia que precisamos trabalhar com algum dado raster, e o PyQGIS nos permite trabalhar com eles também.

Mantendo a mesma proposta, de se ter um diretório base para se trabalhar, vamos adicionar o nosso arquivo raster `BR_SRTM.if` que está dentro da pasta "dados" do repositório.  

Para adicionarmos um raster, vamos criar a camada usando o a classe `QgsRasterLayer()` que cria uma camada do tipo raster.  
Para adicionar, basta utilizar o mesmo método `addMapLayer()` que utilizamos para adicionar as camadas de vetores na aula passada.  

A classe precisa de apenas dois parâmetros para ser criada:  

* caminho do arquivo
* nome a ser utilizado na camada

`QgsRasterLayer( "path" , "name" )`

```python
import os

# Diretório base, lembre-se de trocar de acordo com seu caminho
diretorio = "path_to_livepyqgis"

raster = os.path.join( diretorio, "dados" , "BR_SRTM.tif" )
rlayer = QgsRasterLayer( raster , "BR_SRTM" )

# Adicionando a camada ao projeto
QgsProject.instance().addMapLayer( rlayer )
```

Podemos também utilizar o metodo do `iface`, o `.addRasterLayer( "path" , "name" )`

```python
# Usando iface
iface.addRasterLayer( raster , "BR_SRTM_2" )
```

# O que é esse tal de QgsProject.instance()?

De acordo com o cook book do [PyQgis](https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/loadlayer.html#id3) ele detém a autoridade sobre cada camada e permite que ela possa ser acessada posteriormente em qualquer parte dos script/aplicação que estivermos trabalhando utilizando apenas o id da camada ( `camada_que_queremos.id()`).  

Já vimos antes como adicionar camadas ao projeto utilizando o metodo `addMapLayer()`:  

```python
QgsProject.instance().addMapLayer( rlayer )
```

Há outras formas de adição de camadas:

```python
# Adicionando camadas sem exibição habilitada
QgisProject.instance().addMapLayers( rlayer, False )
```

Para removermos uma camada, usando Python, o método `removeMapLayer()`:  

```python
QgsProject.instance().addMapLayer( rlayer.id() )
```

# Buscando dados do raster

```python
# Pegar resolução do raster
rLayer.width() , rLayer.height()

# Extensão do raster
rLayer.extent()
rLayer.extent().toString()

# Tipo do raster 0 = GrayOrUndefined (single band), 1 = Palette (single band), 2 = Multiband
rLayer.rasterType()

# Contando as bandas do raster
rLayer.bandCount()
```