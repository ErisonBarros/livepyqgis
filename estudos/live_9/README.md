# Live 9: Rodando script em "background" no QGIS

**[![Live pyqgis 9 - Rodando script em background no QGIS](https://img.youtube.com/vi/ymmSijyLK8w/0.jpg)](https://youtu.be/ymmSijyLK8w)**  

## O que é background

Executar um procedimento em `background` consiste em rodar o procedimento numa outra [thread](https://pt.wikipedia.org/wiki/Thread_(computa%C3%A7%C3%A3o)), em vez de executar na mesma `thread` do programa principal.  

Um programa pode ter `threads filhas`, ou seja, procedimentos rodando em background, para isso, o programa principal tem que gerenciar esses threads.  

Ao rodar procedimentos em background, o programa principal fica `livre` para rodar outros procedimentos, no caso  do QGIS, os procedimentos em background não afetam a interface gráfica, ou seja, o usuário pode continuar a navegar no QGIS.

Um bom exemplo de procedimentos em background, é o uso das ferramentas
![ferramentas de processamento](https://gitlab.com/geocastbrasil/livepyqgis/raw/master/estudos/live_9/fig/caixa_ferramenta.png)
Ao rodar uma ferramenta o QGIS não "congela".  

**Como executar procedimentos(scripts) em background no QGIS**

Para executar procedimentos em background, utilizamos a classe [QgsTask](https://qgis.org/api/classQgsTask.html).  

A classe `QgsTask` apenas prepara o procedimento para rodar em background. O procedimento deve ser acoplado numa `QgsTask`.  

Para gerênciar as `tasks`, objetos instanciados da `QgsTask`, utiliza a classe [QgsTaskManager](https://qgis.org/api/classQgsTaskManager.html).  

O `QgsTaskManager` que executa/cancela/... as `tasks`.
<div style="page-break-after: always;"></div>
Para rodar um `script python` numa QgsTask, temos três formas:  
Obs: As duas primeiras é para apenas saber que existe, pois, iremos trabalhar com a última.  

- Criando uma classe a partir da QgsTask.  
A classe herdada tem que ter os métodos que são chamados pela `QgsTaskManager`.  
```python
class SpecialisedTask(QgsTask): ...
    def _init_(self, description, ...):
        super().__init__(description, QgsTask.CanCancel)
        ...

    def run(self):
        # O trabalho(procedimento) é feito aqui
        ...
        return result

    def finished(self, result):
        # Após terminar o 'run' é feita a chamada dessa função
        ...

    def cancel(self):
        # Se for cancelado é feita a chamada dessa função
```

- Criando uma Task utilizando um algoritmo de processamento.  
Para processar uma ferramenta em background, faz o uso das classes de processamento, como, `QgsProcessingAlgRunnerTask`, `QgsProcessingContext`, `QgsProcessingFeedback`, e da função `partial` da `functools`. A ilustração por um pseudo-código desse conjunto de classes não seria oportuno.  

- A partir de uma função utilizando `QgsTask.fromFunction`.  
Essa é a forma que vamos trabalhar e será detalhada a seguir.  

## Uso da QgsTask.fromFunction

A [QgsTask.fromFunction](https://qgis.org/pyqgis/3.0/core/Task/QgsTask.html#qgis.core.QgsTask.fromFunction) possui os seguintes argumentos:
- Descrição da Task('Teste Task').
- Função de processamento(`process`): A função que vai processar em background. Possui obrigatoriamente o argumento `task`.
- Função de finalização(`finished`): Função chamada quando termina a execução da `Função de processamento`. O nome do argumento é `on_finished`. Essa função possui obrigatóriamente os argumentos `exception` e `result`. O `result` é o retorno da `Função de processamento`.
- Demais agumentos(`...`): São os argumentos utilizados na `Função de processamento`.
 <div style="page-break-after: always;"></div>
```python
def process(task, ...):
    # código a ser rodado em bakground
    # Pode receber outros argumentos além de 'task'
    ...
    # Testa se foi cancelado
    if task.isCanceled():
        ...
        return None
    # pode retornar um valor
    return result

def finished(exception, result=None):
    # É chamada ao terminar a You are not allowed to push into this branchfunção 'process'
    # o resultado da função 'process' é passado no result
    # Se tiver uma excessão no 'process' essa é passada em exception
    ...

## Cria uma QgsTask

task = QgsTask.fromFunction('Teste Task', process, on_finished=finished, ...)

## Adicina ao gerênciador de tarefas e executa

QgsApplication.taskManager().addTask( task )
```

### Observações
- Na função `processamento` NUNCA deve usar elementos da interface gráfica, como exemplos, imprimir uma mensagem, adicionar uma camada,....
- A função de `finalização` é executada na thread da aplicação principal(QGIS), assim, ela pode interagir com a interface gráfica. O parâmetro `result` tem que ter o valor default None(result=`None`).  

## Estudo de caso
Script para para gerar um arquivo CSV com:  
- Nome do arquivo
- Caminho do arquivo
- Extensão do arquivo
- Tamanho do arquivo  

### Etapas
- Script rodando na `thread` da aplicação
- Script rodando em backgroud

`Script`: getdescriptionfiles.py

### Fontes
- [Tasks - doing heavy work in the background](https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/tasks.html)
- [Using Threads in PyQGIS3](https://www.opengis.ch/2018/06/22/threads-in-pyqgis3/)

### Ambiente utilizado p/ Live
* Kubuntu 18.04
* QGIS 3.10.1