# Live 10: PyQgis Também Salva

![Live 10](https://i9.ytimg.com/vi/7f-57aUoyrA/mqdefault.jpg?time=1583776368193&sqp=CMiDmvMF&rs=AOn4CLCuu6kryUKELHrSI8Hfs3AtkxGMSg)


> Os exemplos são baseados no [CookBook](https://docs.qgis.org/3.4/en/docs/pyqgis_developer_cookbook/vector.html#creating-vector-layers) com alguma adaptação.

Nós já aprendemos a abrir dados ( Lives 1, 1b, 2 e 3)
Aprendemos criar polígonos (Live 6 e 7).
Mas e se depois que aplicarmos todos esses procedimentos, quisermos persistir nossos dados??
É por isso que vamos aprender a salvar nossos dados em arquivos.
E vamos salvar nossos arquivos em [Geopackage](http://www.geopackage.org/) e também em formato [tiff](https://pt.wikipedia.org/wiki/Tagged_Image_File_Format)

## Tem um minutinho para ouvir sobre tipos de argumentos, senhor?

> Fundamento de [Python](www.python.org) 

[Excript](http://excript.com/python/argumentos-nomeados-posicionais-python.html)
[Live de Python](https://youtu.be/-K38SBdeuys)

No [Python](www.python.org) temos a possibilidade de utilizarmos funções e informar argumentos para obtermos efeitos.
Esses argumentos podem ser de dois tipos:

- Posicionais: São argumentos que tem posição definida a serem informados no momento de invocar a função.

Exemplo:

```Python

def subtracao(a: int, b: int ) -> int:
    """Função que subtrai um numero inteiro (A) de um número inteiro (b) e retorna o resultado. """
    return a-b

subtracao(2, 1)
>>> 1

subtracao(1, 2):
>>> -1
```

- Nomeados: São argumentos que recebem um nome para a passagem de valores, fazendo com que a posição não importe no momento de invocar a função.

Exemplo:

```Python
def subtracao(primeiro_numero: int=None, segundo_numero: int=None) -> int:
    """Função que subtrai um numero inteiro (primeiro_numero) de um número inteiro (segundo_numero) e retorna o resultado. """
    return primeiro_numero - segundo_numero

subtracao(primeiro_numero=2, segundo_numero=1)
>>> 1

subtracao(segundo_numero=1, primeiro_numero=2)
>>> 1
```

Podemos mesclar a utilização de argumentos posicionais e nomeados, mas sempre obedecendo a ordem:

`minha_funcao(valores_posicionais, valores_nomeados='')`

Argumentos posicionais devem vir antes dos nomeados.

Dever de casa: estudem sobre empacotamento e desempacotamento (já mostramos isso bem rápido na live [Paraguaian Catalog on the Fly](https://youtu.be/mWkzXifz5MU) e no video Eduardo Mentes acima)

## Classe QgsVectorFileWriter

Essa [classe](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html) é utilizada para escrevermos vetores em arquivos no disco, sendo o método mais importante, agora, o [writeAsVectorFormat()](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html?highlight=qgsvectorfilewriter#qgis.core.QgsVectorFileWriter.writeAsVectorFormat) que é o método que escreve os dados vetoriais em arquivo.

Esse método permite:

- Criar um novo arquivo com as feições.

- Adicionar feições à um arquivo já existente, substituindo a camada, caso o arquivo/camada com o mesmo nome já exista, essa sem acrescentar campos diferentes que possam existir no arquivo.

- Adicionar as feições a um arquivo já existente, adicionando os dados em uma camada, caso o arquivo/camada com o mesmo nome já exista, e acrescenta novo campos.

- Adicionar as feições à um arquivo já existente em camadas/tabelas diferentes (como geopackage, spatialite...).

Por padrão, essa classe escreve dados vetoriais no formato [Geopackage](http://www.geopackage.org/).

### Mãos a massa

#### Parâmetros Obrigatórios

O método `QgsVectorFileWritter.writeAsVectorFormat(layer, fileName, fileEncoding)` precisa de no minimo 3 parãmetros (obrigatórios) para funcionar:

- layer => É a camada que deseja salvar em arquivo;

- fileName => É o caminho do arquivo (path) deseja salvar o arquivo, não precisa de colocar a extensão, o padrão é `.gpkg` mas utilizando os [drivers](https://gdal.org/drivers/vector/index.html) é possível salvar em outros formatos, como o `ESRI Shapefile`.

- fileEncoding => Aqui informamos o encoding do arquivo a ser utilizado (principalmente quando for um Shapefile). O padrão do GPKG é sempre 'UTF-8'

O método `writeAsVectorFormat()` ao ser utilizado sempre retorna uma tupla contendo 2 dados:

(erro: tipo inteiro, texto )

> Atenção: vamos sempre usar uma camada ativa no nosso projeto para podermos trabalhar.

Exemplo 1:

```Python
__writer = QgsVectorFileWriter.writeAsVectorFormat(layer, fileName, '')

if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

O snipet acima irá criar um arquivo novo, caso não exista, e sobreescrever caso haja um arquivo existente.

#### Parâmetros Opcionais

- destCrs

Um quarto parâmetro, opcional, pode ser passado, que é o SRC de destino, esse parâmetro deve ser uma instância válida da classe [QgsCoordinateReferenceSystem()](https://qgis.org/pyqgis/3.4/core/QgsCoordinateReferenceSystem.html#qgis.core.QgsCoordinateReferenceSystem) isso faz uma transformação da camada para um outro src.

```Python
_writer = QgsVectorFileWriter.writeAsVectorFormat(camada,
                                                  caminho_arquivo,
                                                  '',
                                                  QgsCoordinateReferenceSystem('EPSG:102033')
                                                  )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

- driverName

Nesse parâmetro passamos o nome do [driver](https://gdal.org/drivers/vector/index.html) que queremos usar para gerar o arquivo de saida

```Python
_writer = QgsVectorFileWriter.writeAsVectorFormat(camada,
                                                caminho_arquivo,
                                                'UTF-8',
                                                QgsCoordinateReferenceSystem('EPSG:102033'),
                                                'ESRI Shapefile' # Salva a camada como um shapefile
                                                )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

- onlySelected

Faz apenas o salvamento dos dados selecionados dentro da camada.
Ele, por padrão é configurado como `False`, salvando todos os dados, deve-se passar ele como `True`.

```Python
_writer = QgsVectorFileWriter.writeAsVectorFormat(camada,
                                                caminho_arquivo,
                                                'UTF-8',
                                                QgsCoordinateReferenceSystem('EPSG:102033'),
                                                'ESRI Shapefile', # Salva a camada como um shapefile
                                                True,
                                                )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

### Posicionais X Nomeados

Usando os parametros nomeados, facilita a nossa vida, pois não precisamos passar todos os para apenas informar o que queremos.

Exemplo 1: Salvando uma camada como GPKG, mas penas os selecionados

Posicional -> Devemos passar todos os argumentos.

```Python
_writer = QgsVectorFileWriter.writeAsVectorFormat(camada,
                                                caminho_arquivo,
                                                '',
                                                QgsCoordinateReferenceSystem('EPSG:4674'),
                                                'GPKG,
                                                True,
                                                )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

Nomeados - Usados, principalmente para declarar os parametros opcionais, que queremos utilizar.

```Python
_writer = QgsVectorFileWriter.writeAsVectorFormat(camada, # Obrigatório
                                                caminho_arquivo, # Obrigatorio
                                                '', # Obrigatorio
                                                onlySelected=True
                                                )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

Exemplo 2: Salvando apenas determinados campos da tabela de atributos

metodo `attributes(iteravel[inteiros])` -> passamos uma lista com os indices de campos que queremos.

```Python
# Pegando apenas os campos estado, nome e geocodigo

campos = [2, 4, 6]

_writer = QgsVectorFileWriter.writeAsVectorFormat(layer=camada, # Obrigatório
                                                fileName=caminho_arquivo, # Obrigatório
                                                encoding='', # Obrigatório
                                                attributes=campos # Poderia passar lista direto aqui [2, 4, 6].
                                                )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

## SaveVectorOptions

[SaveVectorOptions](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html#qgis.core.QgsVectorFileWriter.SaveVectorOptions) é uma sub classe que torna possível construir as opções a serem passadas para o `QgsVectorFileWriter` no momento de gravar o arquivo, é com ele que podemos definir comportamentos, tais como:

- Determinar nome da camada a ser criada no arquivo (quando esse permite várias camadas)

- Se a camada existir > Substituir, adicionar dados na camada sem criar novos campos, adicionar dados na camada e criar novos campos.

Para isso, criamos uma instancia dela e passamos os atributos (da sub classe) que queremos utilizar.

Lembrando que na documentação existem muitas opções que podemos usar.

> Usando essa opção, o encoding não é obrigatório

### Mãos à obra

- layerName -> Determina o nome da camada dentro do arquivo.

```Python

opcoes = QgsVectorFileWriter.SaveVectorOptions() # Instancia da Sub Classe
opcoes.layerName = layer.name()

_writer = QgsVectorFileWriter.writeAsVectorFormat(layer=camada, # Obrigatório
                                                fileName=caminho_arquivo, # Obrigatório
                                                opcoes
                                                )

# Verificando existência de erro durante o salvamento
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
```

- onlySelectedFeatures -> salva apenas as feições selecionadas.

```Python

opcoes = QgsVectorFileWriter.SaveVectorOptions() # Instancia da Sub Classe
opcoes.onlySelectedFeatures = True

_writer = QgsVectorFileWriter.writeAsVectorFormat(camada, # Obrigatório
                                                caminho_arquivo, # Obrigatório
                                                opcoes
                                                )
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
else:
    print(_writer)
```

- [actionOnExistingFile](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html#qgis.core.QgsVectorFileWriter.ActionOnExistingFile) -> Determina o comportamento quando o arquivo já existe. deve-se passar um inteiro, ou um atributo da sub classe [ActionOnExistingFile](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html#qgis.core.QgsVectorFileWriter.ActionOnExistingFile).

Sem instancia da sub classe ActionOnExistingFile

```Python
# Adicionando camadas, com sobreescrita quando o nome de camada for o mesmo
# Deve ser aplicado em um arquivo pré existente

opcoes = QgsVectorFileWriter.SaveVectorOptions() # Instancia da Sub Classe
opcoes.layerName = "nome_da_camada"
opcoes.actionOnExistingFile = 1

_writer = QgsVectorFileWriter.writeAsVectorFormat(camada, # Obrigatório
                                                caminho_arquivo, # Obrigatório
                                                opcoes
                                                )
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
else:
    print(_writer)
```

Com instancia da sub classe

```Python
# Adicionando camadas, com sobreescrita
# Deve ser aplicado em um arquivo pré existente

opcoes = QgsVectorFileWriter.SaveVectorOptions() # Instancia da Sub Classe
quandoExistirArquivo = QgsVectorFileWriter.ActionOnExistingFile

opcoes.layerName = "nome_da_camada"
opcoes.actionOnExistingFile = quandoExistirArquivo.CreateOrOverwriteLayer

_writer = QgsVectorFileWriter.writeAsVectorFormat(camada, # Obrigatório
                                                caminho_arquivo, # Obrigatório
                                                opcoes
                                                )
if _writer[0] == QgsVectorFileWriter.NoError:
    print(_writer)
    print("Arquivo Salvo com sucesso!")
else:
    print(_writer)
```

## Classe QgsRasterFileWriter

Essa [classe](https://qgis.org/pyqgis/master/core/QgsRasterFileWriter.html) é utilizada para salvarmos arquivos Rasters (dados matriciais) em disco.

Com ela podemos criar raster de banda simples e multi-bandas e até salvar dentro de um GPKG.
Para isso, basta instanciar a classe passando como parâmetro o endereço da futura imagem.
Para gerarmos dados matriciais (rasters), vamos precisar de um "pipe" onde iremos passar atributos como:

- tipo de provedor;
- projeção.

E no final, basta mandar gravar o arquivo passando os atributos:

- Tamanho no eixo x;
- Tamanho no eixo Y;
- Extensão da matriz a ser gerada;
- SRC da imagem.

```Python
from os import path

camada = iface.activeLayer()
caminho_pasta = 'caminho/da/pasta/do/arquivo/'
caminho_arquivo = path.join(caminho_pasta, "nome_do_arquivo.tif") # Esse é um arquivo que usamos de teste.
salvo = path.join(caminho_pasta, "salvo.tif")

def write(theRasterName):
    path = f"{caminho_pasta}{theRasterName}" # Nome do arquivo de teste.
    rasterLayer = QgsRasterLayer(path,'imagem') # Cria uma camada raster dentro do projeto do qgis,
    QgsProject.instance().addMapLayer(rasterLayer) # Adicionar a Camada ao projeto do qgis.
    if not rasterLayer.isValid(): # Verifica se a camada é válida
        return False

    provider = rasterLayer.dataProvider()

    fileWriter = QgsRasterFileWriter(salvo)

    pipe = QgsRasterPipe()
    if not pipe.set(provider.clone()):
        print("Cannot set pipe provider")
        return False

    projector = QgsRasterProjector()
    projector.setCrs(provider.crs(), provider.crs())

    if not pipe.insert(2, projector):
        print("Cannot set pipe projector")
        return False

    fileWriter.writeRaster(
        pipe,
        provider.xSize(),
        provider.ySize(),
        provider.extent(),
        provider.crs())
    print(fileWriter)

    iface.addRasterLayer(salvo, "salvo") # adicionando o raster gerado

write("2329916_2010-06-12T140237_RE3_3A-NAC_3790448_88525.tif")
```

Existem muitas outras opções de gerar um raster, como já informar um raster com uma determinada renderização.

Uma forma interessante é ler os testes do qgis, para aprender como as coisas funcionam dentro dele.

[Clique aqui](https://github.com/qgis/QGIS/blob/master/tests/src/python/test_qgsrasterfilewriter.py) para ver os testes da classe QgsRasterFileWritter.
