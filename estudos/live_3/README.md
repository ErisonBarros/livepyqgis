# Live 3: Executando algoritmos do processing pelo PyQGIS  
  
**[![Live pyqgis 3 - Catalog on the fly paraguaio](https://img.youtube.com/vi/mcCHHjeDB0o/0.jpg)](https://youtu.be/mcCHHjeDB0o)**  

[Live da Issue 1](https://gitlab.com/geocastbrasil/livepyqgis/issues/1)  

## Dicionários

> O que são? Como funcionan? Para quê servem?
> Hoje na live de PyQGIS.

Aqui nessa live vamos usar uma estrutura de dados nova do [Python](https://www.python.org/) que são dicionários.  
Ele é uma derivação da lista, que vimos na live passada, e funciona um pouquinho diferente.

As listas são compostas por uma __CHAVE__ e seu __SIGNIFICADO__.  
> Não diga??

A chave sempre será do tipo texto, já o seu significado pode ser qualquer tipo.  
Essa é a estrutura de um dicionario:

```Python
# Exemplo
nome_do_dicionario = { 'chave': significado, 'chave 2': significado_2, ..., 'chave N': significado_n}
```

Exemplo:  

```python
aurelio = {'ArcGIS': 'Um dos mais reconhecidos e mais caros SIG do mundo',
			'QGIS': 'Talvez o mais conhecido SIG de código aberto',
			'GRASS GIS': 'Pouco falado, nas um dos mais poderosos que já ouvi falar'}
```

Para acessamos um significado dentro de um dicionario, basta informar qual a chave que queremos acessar:  
`dicionario['chave']` e obteremos como retorno `>> significado`.

Exemplo:

```Python
# Pegando o significado de QGIS
print( aurelio['QGIS'] )
>> Talvez o mais conhecido SIG de código aberto
```

Para adicionar novas chaves e siginificados a um dicionário, ou mudar o siginificado de uma chave, basta informar a chave e qual o significado dela:

```python
# Inserindo uma chave que não exite e seu significado
aurelio['GVSIG'] = 'Deve ser interessante, mas nunca usei'

# Modificando um significado de uma chave
aurelio['QGIS'] = 'É o mais falado... e com #PyQGIS, vira uma bazuca!'
```

Vale muito a pena [ler mais sobre dicionários]((https://docs.python.org/3.5/tutorial/datastructures.html#dictionaries)), pois pode ser um ótimo aliado e será bastante utilizado. Incusive na hora de rodar algoritmos do *processing*.  

## As vias de fatos

Vamos voltar um pouco na [live #1]('../live_1/README.md'), onde carregamos alguns dados (na verdade só um - as Unidades da Federação do Brasil e filtramos a UF do Rio de Janeiro) e salvamos o projeto. Nessa live, vamos ver:  

 1. Como cerregar esse projeto, analisar alguns detalhes básicos da estrutura de simbologia de dados vetoriais (neste caso, polígono);
 2. Carregar um dado raster (altitude SRTM do Brasil. A mesma carregada na live #2 - [Script2](../live_2/Script2.py) );  
 3. "Meter a mão no processing" (!):  
        1. Projetar um dado vetorial;  
        2. Criar um *hillshade*;  
        3. Criar um dado de declividade (*slope*), ambos a partir do raster SRTM;

Isso tudo vendo alguns detalhes de cada execussão e algumas possibilidades que nos brinda o fato de estarmos usando **PyQGIS**.  

### Alguns detalhes antes de começar

Só para lembrar, estamos usando o módulo `os` para facilitar a gestão de `path`. Na verdade para padronizar, já que cada um deve estar usando uma pasta diferente. Para mais informações veja a documentação do módulo [os](https://docs.python.org/3.5/library/os.html).  
E temos uma novidade nesta live: vamos usar um módulo python novo: [`pprint`](https://docs.python.org/3.5/library/pprint.html). Ele nos facilitará para fazer `print` de alguns elementos de uma forma já formatada ("organizada", mesmo sabendo que "organização", pode ser algo subjetivo :)  
Vamos lá, carregando os módulos e definindo o path para a pasta onde se encontra o resposiótio `livepyqgis`:

```python
import os
from pprint import pprint
diretorio = "path_to_livepyqgis"
```  

### Carregar un projeto já existente

Bom, vimos na [live 1](../live_1/README.md) o método `QgsProject.instance().write()` para salvar o projeto que estivemos usando.  
> Adivinha só?!

Existe um método chamado [`QgsProject.instance().read()`](https://qgis.org/pyqgis/3.0/core/Project/QgsProject.html?#qgis.core.QgsProject.read) para carregar um projeto já salvo :)  
Vamos então carregar o projeto salvo:  

```python
# Abrindo o projeto salvo na aula anterior
QgsProject.instance().read(os.path.join(diretorio, 'projetos/projeto1.qgs'))
```  

Bom, como já havíamos carregado uma camada vetorial e até estivemos "brincando" com a simbologia dela, vamos explorar mais algumas coisas relacionadas a simbologia.
É importante perceber que, mesmo que tenhamos carregado um projeto, o ambiente python é novo. Por isso, o `objeto` que tinhamos criado chamado `uf` já não existe mais. Por isso, vamos criá-lo novamente para poder manipula-lo.  

```python
# acessando layer
uf = QgsProject.instance().mapLayersByName('Estados Brasil lim_unidade_federacao_a')[0]
```  

Já havíamos visto o método [`mapLayersByName`](https://qgis.org/pyqgis/master/core/QgsProject.html?highlight=maplayersbyname#qgis.core.QgsProject.mapLayersByName) de acessar uma camada pelo nome na [live #2](../live_2/README.md).  
Agora poderemos acessar as propriedades da simbologia da camada com o método [`properties`](https://qgis.org/pyqgis/master/core/QgsSymbolLayer.html?highlight=symbollayer#qgis.core.QgsSymbolLayer.properties) da classe [`QgsSymbolLayer`](https://qgis.org/pyqgis/master/core/QgsSymbolLayer.html?highlight=symbollayer#module-QgsSymbolLayer).  

### Propriedades da simbologia ( e dicionários :)  

Durante a live #1 fizemos alguns experimentos relacionados à simbologia da camada vetorial. Mas a verdade é que não chegamos a apresentar as propriedades da simbologia. É a partir dessa propriedade que será possível alterar, por exemplo, o tipo de preenchimento da camada (em se tratando de uma camada de polígonos).  
Vamos dar uma olhada nessas propriedades:  

```python
# identificando propriedades de simbologia
uf.renderer().symbol().symbolLayer(0).properties()
# resultado
>> {'border_width_map_unit_scale': '3x:0,0,0,0,0,0', 'color': '0,0,0,255', 'joinstyle': 'bevel', 'offset': '0,0', 'offset_map_unit_scale': '3x:0,0,0,0,0,0', 'offset_unit': 'MM', 'outline_color': '35,35,35,255', 'outline_style': 'solid', 'outline_width': '0.26', 'outline_width_unit': 'MM', 'style': 'solid'}
```

É aqui que o pprint faz a diferença... (é só visual.... mas vale a pena!)

```python
pprint(uf.renderer().symbol().symbolLayer(0).properties())
# resultado
>> {'border_width_map_unit_scale': '3x:0,0,0,0,0,0',
 'color': '0,0,0,255',
 'joinstyle': 'bevel',
 'offset': '0,0',
 'offset_map_unit_scale': '3x:0,0,0,0,0,0',
 'offset_unit': 'MM',
 'outline_color': '35,35,35,255',
 'outline_style': 'solid',
 'outline_width': '0.26',
 'outline_width_unit': 'MM',
 'style': 'solid'}
```  

Mas e então? de que se trata isso?  

### Estrutura de simbologia da camada vetorial

As propriedade da simbologia são estruturados no formato [`dicionário`](https://docs.python.org/3.5/tutorial/datastructures.html#dictionaries), que a gente viu agora no começo da live. E acho que não haveria estrutura de dados mais conveniente...

Voltando às propriedades de simbologia da camada vetorial: ela possui parâmetros (que estão as chaves '`keys`' do dicionário) e seus respectivos valores estão com valores padrão/aleatório. Como tais valores são passíveis de alteração... vamos lá:
Vamos alterar o **preenchimento** para "**sem pincel**" (conforme tradução para PT-BR).  
Só para facilitar, vou atribuir o resultado a uma variável `dic`. Vou acessar o estilo de preenchimento `dic["style"]` e alteralo simplesmente para o valor "no".

```python
# salvando esse dicionario para alterar um parametro
dic = uf.renderer().symbol().symbolLayer(0).properties()

# visualizando a diferenca do print e pprint
pprint(dic)
print(dic)

# alterando parametro para transparente
dic["style"] = "no"
```

Veja, com isso poderíamos alterar qualquer valor do dicionário... por exemplo, a cor da linha de borda:
`dic["outline_color"] = '255,255,255,255'`.  
Depois basta atualizar a camada (afinal, até o momento estivemos trabalhando e alterando apenas a variável `dic`):  

```python
# fazendo update
uf.renderer().setSymbol(QgsFillSymbol.createSimple(dic))
# atualizando canvas
uf.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(uf.id())
```  

### Adicionando camada raster

Apesar de já termos feito isso na [live #2](../live_2/README.md), faremos de novo já que o projeto carregado não possui nenhuma camada raster e pretendemos utilizá-la no `processing`.  
Vamos lá: adicionando o raster de altitude para o Brasil:  

```python
# add raster layer
iface.addRasterLayer(os.path.join(diretorio, "dados/BR_SRTM.tif"))
```  

### Atribuir a uma variável uma camada com mapLayersByName()  

Agora, vamos criar e atribuir a variável `rlayer` a camda adicionada recentemente. Porém, agora vamos usar o método [`QgsProject.instance().mapLayersByName('<nome da camada>')`](https://qgis.org/pyqgis/master/core/QgsProject.html?highlight=maplayersbyname#qgis.core.QgsProject.mapLayersByName)  

```python
# Para acessa layer por nome:
rlayer = QgsProject.instance().mapLayersByName('BR_SRTM')[0]
```  

Agora, sim. Acho que temos tudo para explorar o processing...  

### Executando algoritmos do Processing

Antes de sair "metendo a mão" no processing, vamos dar uma olhada em algumas coisas...
**Quantos algoritmos temos?**
Vamos usar o método [`processingRegistry()`](https://qgis.org/pyqgis/master/core/QgsApplication.html?highlight=processingregistry#qgis.core.QgsApplication.processingRegistry) da classe `QgsAplication()` para acessar essa informação e outras:  

```python
# quantos algoritos temos?
len(QgsApplication.processingRegistry().algorithms())
# resultado 542
```

Agora vamos dar uma olhada nos dez (só para não ficar uma lista enorme) primeiros algoritmos disponíveis.

```python
# quais sao eles?
pprint(QgsApplication.processingRegistry().algorithms()[0:10])
```

Para mais informações sobre o [`processingRegistry()`](https://qgis.org/pyqgis/master/core/QgsProcessingRegistry.html?highlight=processingregistry#module-QgsProcessingRegistry) veja sua documentação.  
Percabe que não há como executar os algoritmos... para isso vamos importar o módulo `processing`.  

```python
# usando processing
import processing
```  

### Executando nosso primeiro algoritmo (`qgis:reprojectlayer`)

Vale a pena darmos uma olhada num método que imagino ser um potencial companheiro em nossa jornada... o `algorithmHelp(<nome_algorithm>)`. Ao executá-lo teremos todas as informações pertinentes e necessárias para a **boa** execussão do algoritmo em questão.
Vamos ver o algoritmo para projetar uma camada vetorial?  

```python
processing.algorithmHelp("qgis:reprojectlayer")
# resultado
>> Reproject layer (native:reprojectlayer)

This algorithm reprojects a vector layer. It creates a new layer with the same features as the input one, but with geometries reprojected to a new CRS.

Attributes are not modified by this algorithm.


----------------
Input parameters
----------------

INPUT: Input layer

	Parameter type:	QgsProcessingParameterFeatureSource

	Accepted data types:
		- str: layer ID
		- str: layer name
		- str: layer source
		- QgsProcessingFeatureSourceDefinition
		- QgsProperty
		- QgsVectorLayer

TARGET_CRS: Target CRS

	Parameter type:	QgsProcessingParameterCrs

	Accepted data types:
		- str: 'ProjectCrs'
		- str: CRS auth ID (e.g. 'EPSG:3111')
		- str: CRS PROJ4 (e.g. 'PROJ4:...')
		- str: CRS WKT (e.g. 'WKT:...')
		- str: layer ID. CRS of layer is used.
		- str: layer name. CRS of layer is used.
		- str: layer source. CRS of layer is used.
		- QgsCoordinateReferenceSystem
		- QgsMapLayer: CRS of layer is used
		- QgsProcessingFeatureSourceDefinition: CRS of source is used
		- QgsProperty

OUTPUT: Reprojected

	Parameter type:	QgsProcessingParameterFeatureSink

	Accepted data types:
		- str: destination vector file, e.g. 'd:/test.shp'
		- str: 'memory:' to store result in temporary memory layer
		- str: using vector provider ID prefix and destination URI, e.g. 'postgres:...' to store result in PostGIS table
		- QgsProcessingOutputLayerDefinition
		- QgsProperty

----------------
Outputs
----------------

OUTPUT:  <QgsProcessingOutputVectorLayer>
	Reprojected
```  

Fica claro a riqueza e a boa organização desse help. Por isso irei atentar a um, dos vários pontos interessantes.... o OUTPUT, por exemplo.
Veja que ele informa o tipo de output aceito por esse algoritmo. E dentre as opções, temos 'memory:', que deverá ser usado quando quisermos armazenos o resultado como temporário na memória.

:warning: **Nem todos algoritmos aceitam essa opção**. Por isso, atenção. Vale a pena ler sempre o *help* do algoritmo.  

E já que esse algorimo aceita, vamos prová-lo?!

```python
# executando
projectResult = processing.run('qgis:reprojectlayer', {
    'INPUT': uf,
    'TARGET_CRS': 'EPSG:102033',
    'OUTPUT': "memory:ufProjected"})
```

Reparem que para rodar, basta usar o método `processing.run()`. O primeiro parâmetro deve ser o *provider* seguido do nome do algoritmo. E para as configurações do algoritmo... ele, claro, o dicionário informando os parâmetros (*keys*) e seus respectivos valores!  

> Ótimo, o algoritmo rodou. Mas como faço para adicionar o resultado ao projeto?
 
Como criamos uma variável que recebe o resultado, podemos acessar o parâmetro (nesse caso, `key`) `OUTPUT`, e usar o `addMapLayer` para adicioná-lo ao projeto.  

```python
projectResult['OUTPUT']
ufProjected = QgsProject.instance().addMapLayer( projectResult['OUTPUT'] )
```

Só para confirmar que o algoritmo rodou direito, vamos acessar o `crs()` da camada:  

```python
# vendo CRS
ufProjected.crs().authid()
# resultado
>> 'EPSG:102033'
```  

### Hillshade: Carregando após processamento (`processing.runAndLoadResults`)

Com a projeção da camada vetorial, primeiro rodamos o algoritmo, depois carregamos ao projeto. Mas há a possibilidade de rodar o algoritmo e já carregá-lo ao projeto, com o étodo `runAndLoadResults()`.  
Vamos usá-lo no algoritmo de criação do *hillshade*, tendo como input o raster SRTM do Brasil. Esse algoritmo não permite ter o output como memória. Como não pretendo tê-lo no PC, vou salvá-lo na pasta temporária (*tmp*):  

```python
# processando e carregando

# hillshade
processing.algorithmHelp("qgis:hillshade")
output = os.path.join('/tmp/processing_hillshade.tif')
hillshade = processing.runAndLoadResults('qgis:hillshade', {
    'INPUT': rlayer,
    'Z_FACTOR': 111120,
    'AZIMUTH': 300,
    'V_ANGLE': 40,
    'OUTPUT': output})
```

Sobre o [`Z_FACTOR`, recomendo leitura do blog da Aninta Graser](https://anitagraser.com/2012/01/19/a-guide-to-beautiful-reliefs-in-qgis/).


Vamos experiementar outro algoritmo, agora tendo o *gdal* como *provider*:

### Slope (`gdal:slope`)  

```python
# slope
processing.algorithmHelp("gdal:slope")
output = os.path.join('/tmp/processing_slope.tif')
out = processing.runAndLoadResults("gdal:slope",
    {'INPUT': rlayer,
    'BAND': 1,
    'SCALE': 111120,
    'AS_PERCENT': False,
    'COMPUTE_EDGES': False,
    'ZEVENBERGEN': False,
    'OUTPUT': output
    })
```

Para mais informações, dê outra olhada no blog da Anita Graser [sobre rodar algoritmos do processing](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/pyqgis-101-running-processing-tools/)  
